# service2service

> A service to service authentication pattern implemented as a set of modules
> Author: Seth Moeckel <seth.moeckel@oliveai.com>

## Setting up environments for use with this library
service2service relies on Hashicorp Vault for storing secrets (api keys), which means Vault will require some setup in order to work properly.

`secret/${hostService}/${process.env.ENV}/service2service/${clientService}/creds`

TODO: include a setup script

## Using the Go module
Installation of the Go module is as simple as:
```sh
go get bitbucket.org/crosschx/service2service/go/service2service
```

## Using the Node module
Installation of the Node module should be pretty easy (as long as you're connected to Bonn)
```sh
npm i -S service2service
```